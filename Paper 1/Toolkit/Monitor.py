#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pickle
import numpy as np
from .Box import *

class Monitor(object):

    def __init__(self, abs_type, netName, class_y, layer_i, good_ref=None, bad_ref=None):
        self.abs_type = abs_type
        self.netName = netName
        self.classification = class_y
        self.location = layer_i
        self.good_ref = good_ref
        self.bad_ref = bad_ref


    def set_reference(self, good_ref, bad_ref):
        self.good_ref = good_ref
        self.bad_ref = bad_ref
    
    def get_identity(self):
        print("Monitor for network:" + self.netName + "class: " + str(self.classification) + "at layer " + str(self.location))


    def make_verdicts(self, features):
        in_good_ref = []
        in_bad_ref = []
        if len(self.good_ref) and len(self.bad_ref):
            in_good_ref = ref_query(self.abs_type, features, self.good_ref)
            in_bad_ref = ref_query(self.abs_type, features, self.bad_ref)
            
        elif (not len(self.good_ref)) and len(self.bad_ref):
            in_good_ref = [False for x in features]
            in_bad_ref = ref_query(self.abs_type, features, self.bad_ref)
        
        elif len(self.good_ref) and (not len(self.bad_ref)):
            in_good_ref = ref_query(self.abs_type, features, self.good_ref)
            in_bad_ref = [False for x in features]

        else:
            in_good_ref = [False for x in features]
            in_bad_ref = [False for x in features]

        verdicts = query_infusion(in_good_ref, in_bad_ref)
        return verdicts

def ref_query(abs_type, features, reference):
    abstraction_types = {"Box": boxes_query}
    if abs_type in abstraction_types:
        query_results = [abstraction_types[abs_type](x, reference) for x in features]
        # query_results = [boxes_query(x, reference) for x in features]
        return query_results
    else:
        print("Undefined type of abstraction")


def query_infusion(in_good_ref, in_bad_ref):
    if len(in_good_ref) == len(in_bad_ref): #0: acceptance (true, false), 1: rejection (false, true or false), 2: uncertainty (true, true)
        verdicts = np.zeros(len(in_good_ref), dtype=int)
        for i in range(len(in_good_ref)):
            if not in_good_ref[i]:
                verdicts[i] = 1
            elif in_bad_ref[i]:
                verdicts[i] = 2
        return verdicts
    else:
        print("Error: IllegalArgument")       
