#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import numpy as np
import pandas as pd
from .Box import *
from .Monitor import *



def monitors_offline_construction(network_folder_path, classes, taus):
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]

    for y in classes:
        
        # load obtained features to creat reference
        path_bad_features = network_folder_path + "/class_" + str(y) + appendixes[0]
        path_good_features = network_folder_path + "/class_" + str(y) + appendixes[1]
        bad_feat_clustering_results = []
        good_feat_clustering_results = []


        if os.path.exists(path_bad_features):
            bad_features_to_cluster = np.genfromtxt(path_bad_features, delimiter=',')
            # load clustering results to partition the features
            bad_feat_clustering_results_path = network_folder_path + "/clustering_results_class_" + str(y) + appendixes[0]
            if os.path.exists(bad_feat_clustering_results_path):
                bad_feat_clustering_results = pd.read_csv(bad_feat_clustering_results_path)

        if os.path.exists(path_good_features):
            good_features_to_cluster = np.genfromtxt(path_good_features, delimiter=',')
            # load clustering results to partition the features
            good_feat_clustering_results_path = network_folder_path + "/clustering_results_class_" + str(y) + appendixes[1]
            n_dim = good_features_to_cluster.shape[1]
            if os.path.exists(good_feat_clustering_results_path):
                good_feat_clustering_results = pd.read_csv(good_feat_clustering_results_path)

        for tau in taus:
            good_loc_boxes = []
            bad_loc_boxes = []

            if len(bad_feat_clustering_results):
                # load clustering result related to tau
                bad_feat_clustering_result = bad_feat_clustering_results[str(tau)]
                # determine the labels of clusters
                bad_num_clusters = np.amax(bad_feat_clustering_result) + 1
                bad_clustering_labels = np.arange(bad_num_clusters)
                
                # extract the indices of vectors in a cluster
                bad_clusters_indices = []
                for k in bad_clustering_labels:
                    bad_indices_cluster_k, = np.where(bad_feat_clustering_result == k)
                    bad_clusters_indices.append(bad_indices_cluster_k)
                
                # creat local box for each cluster
                bad_loc_boxes = [Box() for i in bad_clustering_labels]
                for j in range(len(bad_loc_boxes)):
                    bad_loc_boxes[j].build(n_dim, bad_features_to_cluster[bad_clusters_indices[j]])
                

            if len(good_feat_clustering_results):
                # load clustering result related to tau
                good_feat_clustering_result = good_feat_clustering_results[str(tau)]
                # determine the labels of clusters 
                good_num_clusters = np.amax(good_feat_clustering_result) + 1
                good_clustering_labels = np.arange(good_num_clusters)
                
                # extract the indices of vectors in a cluster
                good_clusters_indices = []
                for k in good_clustering_labels:
                    good_indices_cluster_k, = np.where(good_feat_clustering_result == k)
                    good_clusters_indices.append(good_indices_cluster_k)    
                
                # creat local box for each cluster
                good_loc_boxes = [Box() for i in good_clustering_labels]
                for j in range(len(good_loc_boxes)):
                    good_loc_boxes[j].build(n_dim, good_features_to_cluster[good_clusters_indices[j]])

            # creat the monitor for class y
            monitor_y = Monitor("Box", "networks", y, 1, good_ref=good_loc_boxes, bad_ref=bad_loc_boxes)
            monitor_stored_path = network_folder_path + "../Monitors/" + "monitor_for_class_" + str(y) + "_tau_" + str(tau) + ".pkl"
            os.makedirs(network_folder_path + "../Monitors/", exist_ok=True)
            with open(monitor_stored_path, 'wb') as f:
                pickle.dump(monitor_y, f)
