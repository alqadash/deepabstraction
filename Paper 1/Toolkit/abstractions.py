import numpy as np
import pandas as pd
import os
import time
from pickle import load
import pickle
import glob

from .Monitor import *
from .Box import *

# import the clustering function
from .cluster_high_level_features import cluster_existed_features

#import the monitor offline construction function
from .monitor_construction import monitors_offline_construction

import warnings
warnings.filterwarnings('ignore')

def construct_monitors(dataset_name, taus, classes=range(10)):

    monitor_folder = "./construction_data/" + dataset_name + "/training/Monitors/"

    build_abstraction(dataset_name,taus,classes, monitor_folder)
    
    monitor_verdicts(dataset_name,taus,classes, monitor_folder)


def build_abstraction(dataset_name,taus,classes, monitor_folder):
    
    feature_folder_path = "./construction_data/" + dataset_name + "/training/high_level_features/"
   
    feature_numpy_folder_path = "./construction_data/" + dataset_name + "/training/numpy/"

    
    

    os.makedirs(feature_numpy_folder_path, exist_ok=True)
    
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]
    
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]

    for y in classes:
        for appendix in appendixes:
            
            high_features_file_path = feature_folder_path + "class_" + str(y) + appendix
            
            numpy_file_path = feature_numpy_folder_path + "class_" + str(y) + appendix
            
            df = pd.read_csv(high_features_file_path)
            
            features_array = df[df.columns[3:13]].to_numpy()
            
            np.savetxt(numpy_file_path, features_array, delimiter=',')

    #Clustering        
    cluster_existed_features(feature_numpy_folder_path, classes, taus)

    #build monitors
    monitors_offline_construction(feature_numpy_folder_path, classes, taus)
    for filename in glob.glob(feature_numpy_folder_path+"clustering*"): # remove temp files
        os.remove(filename) 
        


def monitor_verdicts(dataset_name,taus,classes, monitor_folder):
    
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]
    test_feature_path = "./construction_data/" + dataset_name + "/testing/high_level_features/"
    test_verdict_folder = "./monitors_verdict/" + dataset_name
    os.makedirs(test_verdict_folder, exist_ok=True)
    
    
    for y in classes: # num_classes

        test_feature_bad_path = test_feature_path  + "/class_" +str(y) + appendixes[0]
        test_feature_good_path = test_feature_path  + "/class_" +str(y) + appendixes[1]

        # load test features from csv files 
        df_bad = pd.read_csv(test_feature_bad_path)
        array_test_bad_features = df_bad[df_bad.columns[3:13]].to_numpy()

        df_good = pd.read_csv(test_feature_good_path)
        array_test_good_features = df_good[df_good.columns[3:13]].to_numpy()

        for tau in taus:
            monitor_path = monitor_folder + "monitor_for_class_" + str(y) + "_tau_" + str(tau) + ".pkl"
            with open(monitor_path, 'rb') as f:
                monitor = load(f)
            verdicts_bad = monitor.make_verdicts(array_test_bad_features)
            verdicts_good = monitor.make_verdicts(array_test_good_features)

            # add verdicts of this monitor to the file of verdicts
            column_key = "verdict_" + str(tau)
            df_bad[column_key] = verdicts_bad
            df_good[column_key] = verdicts_good


        # save the obtained verdicts
        df_bad.drop(df_bad.columns[3:13], axis=1, inplace=True)
        df_good.drop(df_good.columns[3:13], axis=1, inplace=True)

        verdicts_bad_file_path = test_verdict_folder + "/verdicts_class_" +str(y) + appendixes[0]
        verdicts_good_file_path = test_verdict_folder + "/verdicts_class_" +str(y) + appendixes[1]

        df_bad.to_csv(verdicts_bad_file_path, index = False)
        df_good.to_csv(verdicts_good_file_path, index = False)






